import Vuex from 'vuex'
import Cookie from 'js-cookie'
// import { threadId } from 'worker_threads';
const createStore = () => {
    return new Vuex.Store({
        state:{
            loadedPosts: [],
            token: null
        },
        mutations: {
            setPosts(state, posts){
                state.loadedPosts = posts
            },
            addPost(state, post){
                state.loadedPosts.push(post)
            },
            editedPost(state, editedPost){
                const postIndex = state.loadedPosts.findIndex(post => post.id === editedPost.id)
                state.loadedPosts[postIndex] = editedPost
            },
            setToken(state, token){
                state.token = token
            },
            clearToken(state){
                state.token = null
            }
        },
        actions: {
            nuxtServerInit(vuexContent, context){
                return context.app.$axios.$get('/posts.json')
                .then(data => {
                    console.log(data)
                    const postArray = []
                    for(const key in data){
                        postArray.push({ ...data[key], id: key })
                    }
                    vuexContent.commit('setPosts', postArray)
                })
                .catch(e => context.error(e))
            },
            setPosts(vuexContent, posts){
                vuexContent.commit('setPosts', posts)
            },
            addPost(vuexContent, post){
                const newPost = {
                    ...post,
                    updatedDate: new Date()
                }
                return this.$axios.$post('/posts.json?auth=' + vuexContent.state.token, newPost)
                .then(data => {
                    vuexContent.commit('addPost', {...newPost, id: data.name})
                })
                .catch(e => console.log(e)) 
            },

            editedPost(vuexContent, post){
                return this.$axios.$put('/posts/' + post.id +'.json?auth=' + vuexContent.state.token, post)
                .then(data => {
                    vuexContent.commit('editedPost', post)
                    console.log(data)})
                .catch(e => console.log(e))
            },
            authUser(vuexContent, userData){
                let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key='
                if(userData.isLogin){
                    url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key='
                }
                return this.$axios.$post( url + process.env.fbApiKey, {
                    email: userData.email,
                    password: userData.password,
                    returnSecureToken: true
                    }).then(res => {
                        vuexContent.commit('setToken', res.idToken)
                        localStorage.setItem('token', res.idToken)
                        localStorage.setItem('tokenExpiration', new Date().getTime() + Number.parseInt(res.expiresIn) * 1000)
                        Cookie.set('jwt', res.idToken)
                        Cookie.set('expiration', new Date().getTime() + Number.parseInt(res.expiresIn) * 1000)
                        return this.$axios.$post('http://localhost:3000/api/track-data',{
                            data: 'Authenticated!'
                        })
                    }).catch(error => {
                        console.log(error)
                    })                
            },
            initAuth(vuexContent, req){
                let token;
                let expirationDate;
                if(req){
                    if(!req.headers.cookie){
                        return
                    }else{
                        const jwtCookie = req.headers.cookie
                            .split(';')
                            .find(c => c.trim().startsWith('jwt='))
                        if(!jwtCookie){
                            return
                        }
                        token = jwtCookie.split('=')[1];
                        expirationDate = req.headers.cookie
                            .split(';')
                            .find(c => c.trim().startsWith('expiration='))
                            .split('=')[1];
                    }
                }else if(process.client){
                    token = localStorage.getItem('token')
                    expirationDate = localStorage.getItem('tokenExpiration')
                }
                if(new Date() > +expirationDate || !token){
                    vuexContent.dispatch('logout');
                }
                vuexContent.commit('setToken', token)
            },
            logout(vuexContent){
                vuexContent.commit('clearToken')
                Cookie.remove('token')
                Cookie.remove('expiration')
                if(process.client){
                    localStorage.removeItem('token')
                    localStorage.removeItem('tokenExpiration')

                }
                
            }
        },
        getters: {
            loadedPosts(state){
                return state.loadedPosts
            },
            isAuth(state){
                return state.token != null
            }
        }
    })
}

export default createStore