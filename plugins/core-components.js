import Vue from 'vue'

import AppControlInput from '@/components/UI-Components/AppControlInput'
import AppButton from '@/components/UI-Components/AppButton'
import PostList from '@/components/Posts/PostList'

Vue.component('AppControlInput', AppControlInput)
Vue.component('AppButton', AppButton)
Vue.component('PostList', PostList)