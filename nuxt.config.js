import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import pkg from './package'
const bodyParser = require('body-parser')
const axios = require('axios')

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Raleway&display=swap'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fa923f' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '~plugins/core-components.js',
    '~plugins/date-filter.js',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios'
  ],

  axios: {
    baseURL: process.env.BASE_URL || 'https://nuxt-blog-778f2.firebaseio.com/'
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },

  env:{
    fbApiKey: 'AIzaSyBCYDp7ke1Mphe7NKwvvahkv41WM2bj1-U'
  },

  serverMiddleware:[
    bodyParser.json(),
    '~/api'
  ],

  generate:{
    routes: function(){
      return axios.get('https://nuxt-blog-778f2.firebaseio.com/posts.json')
        .then(res => {
          const routes = []
          for(const key in res.data){
            routes.push({
              route: '/posts/'+key,
            payload: {postDara: res.data[key]}
            })
          }
          return routes
        })
    }
  }
    
  
}
